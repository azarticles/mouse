# Results

Current state of the project is WIP (work in progress) and for now
only results of detection and tracking could be presented.
For testing proposed tool, were used videos of different mice.
Output of our tool are three files:

* Video file with whole test process ([@fig:begin]; [@fig:end]);
* Image file with mouse's path ([@fig:path]).
* File with values of computed metrics:
	- Time spent for the platform search;
	- Time spent in all sectors;
	- Time spent in a sector with the platform ([@fig:metrics]);

<div id="fig:result">
![Start of the videoprocessing](fig/video_beg.jpg){#fig:begin width=24%}
![End of the videoprocessing](fig/video_end.jpg){#fig:end width=24%}

![Tracked path of the observed mouse](fig/path.jpg){#fig:path width=24%}
![Calculated metrics](fig/metrics.png){#fig:metrics width=24%}

The processing of "Morris Water Maze" test
	
</div>


During processing of the video detecting and tracking of the mouse
occurs in real time.
If there are some false positives during detection, they are not
tracking until they are persistent for *n* amount of frames.
If this false positives are persistent and their tracking started,
they are not taken into account until their tracking path is not
the longest one.

As was mentioned previously, current state is WIP.
In the next phase of our project different metrics will be measured.
Some of them are:

* Gallagher's proximity - the average distance of the subject
	from the platform, corrected for start point using the average 
	swim speed for the trial and removing the time required to swim
	at that speed to the platform position, from the beginning of the
	trial data, so as to exclude that part of the trial from 
	the measure [@gallagher];
* How many times the mouse crosses the platform;
* Whishaw's index - measure of how straight the path of the
	mouse is from the starting point to the platform.
* Platform search strategy. The swimming path of animals is
	first divided into segments, and then the segments are classified 
	into behavioral strategies. 
	Thus, it is possible to track changes in the behavior of animals
	with each trial, and the way of swimming of animals in general is
	divided into more than one strategy, revealing how their
	behavior changes.
