# Related Work

During the Morris Water Maze the behavior of animals is commonly recorded in 
either a manual or a semiautomatic way.
Traditionally, a researcher observes the animal; if the researcher considers that
a certain behavior pattern is displayed, the researcher writes down the behavior, 
either by hand or by entering the data into an event-recording program [@ethovision].
Manual recording of behavior can be implemented with a relatively low investment,
and, for some behaviors, it may be the only way to detect and record their
occurrence.
However, automated observation can provide significant advantages.
Behaviors are recorded more reliably because computer algorithm works in the
same way, and the system does not suffer from observer fatigue or drift.
For instance, in contrast to manual observation, video tracking carries out
pattern analysis on a video image of the observed animals to extract 
quantitative measurements of the animals' behavior.
Automated observation using video tracking is particularly suitable for
measuring locomotor behavior, expressed as spatial measurements
(distance, speed, turning etc.) that the human observer is unable to accurately
estimate [@moubeat].

Nowadays the behavioral test "Morris Water Maze" is practically not used without
automated analysis of the received data, as the only parameter that can be 
manually registered is the latent time of finding the platform.
For a scientist there are a lot of commercial and well tested automated programs
for the analysis of behavior in rodents have been developed.
The main drawback of these programs is their cost.
Most of them are sold as both hardware and software packages, such approach
makes these programs unavailable for many laboratories.
Another drawback of these tools is a small amount of set of output parameters
which are usually limited by latent time, percentage time in quadrant and the 
zone of interest, and sometime a Whishou error.
Examples of such automated programs for analysis of rodent behavior are
EthoVision [@ethovision], Smart Video Tracking Software [@panlab],
VideoTrack [@videotrack] and AnyMaze [@anymaze].
Most of them are sold as a hardware and software package, making these options
unaffordable for many laboratories.

However, there is another approach - open-source software developed by another
scientists.
The advantages of such programm are absolutely simple and inexpensive way to
purchase.
But there are also possible drawbacks as low quality of analysis, complicated
interface, selective specialization and low approbation.
Also, some of the publicly available software options need a MATLAB 
license [@araga], which results in an increased cost, take much longer to run or
are limited to the operating systems.
There is an open-source Mouse Behavioral Analysis Toolbox (MouBeAT) [@moubeat]
which is based on the image analysis software ImageJ.
Another one open-source solution is ToxTrac, which was developed by a group of
Swedish scientists in 2017 [@toxtrac].
This solution did not suit us, as during analysis of video files the trajectory
was not determined correctly.

After research it was decided to develop such software ourselves.
In this article we present a self-contained tool for Morris Water Maze 
test automatization.
As it is written on C++ and Rust programming languages and is self-contained,
this program is easy to install.
It does not require additional software to be installed.
