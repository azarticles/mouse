# Materials and Methods

## Experimental Setup 

The test setup is a polypropylene water pool mounted on a steel base with 
adjustable supports (Open Science, TS1004-M2).
Pool diameter 1.5 m, wall height 60 cm.
The pool package includes a height-adjustable white acrylic platform with 
a metal weighting in the base, 10 cm in diameter.
The unit is additionally equipped with a digital video system VS 1304-1 with a
portable telescopic tripod. 
The video system consists of a highly sensitive digital video camera GigE Vision
(DMK23GV024) and a Fujinon lens (YV5x2.7R4B-2).
Video recording during the experiment is controlled from a stationary computer 
using a free Gigabit Ethernet interface.

## Preprocessing

Videos were acquired in DVVIDEO codec at 30 frames per second (fps) and for further
work with them, they have to be reencoded. 
Our tool provides two functions to reencode DVVIDEO to H.265 codec, either individually
or in batch.
These functions are described in library files written on Rust programming language.
For its correct work the open source software FFmpeg is required to be available 
on the user’s computer.
This software could be selected to install with the tool.

For mouse detection there are several preprocessing steps.
First is pool detection to reduce area of movement. 
The second is background subtraction to remove uninteresting movements.
The third is reducing uninteresting details with blur usage.

### Pool detection

For pool detection on the image modification of the Hough transform was used.
The Hough transform is a feature extraction technique used in image analysis,
computer vision, and digital image processing [@hough].
The purpose of the technique is to find imperfect instances of objects within a certain
class of shapes by a voting procedure.
This voting procedure is carried out in a parameter space, from which object candidates
are obtained as local maxima in a so-called accumulator space that is explicitly
constructed by the algorithm for computing the Hough transform.

### Background subtraction

Background subtraction is a major preprocessing step in many vision-based applications.
In our case the mouse has to be extracted from the static pool.
Technically, the moving foreground has to be extracted from static background.

If there is an image of background alone, without any objects, it is not so hard
to extract an object.
It is enough to subtract the new image from the background.
In most of the cases, there is no such an image, so the background has to be 
extracted from images.
It becomes more complicated when there are some shadows of the object.
Since shadows also move, simple subtraction will mark that also as foreground.

In this work a Background/Foreground Segmentation Algorithm 
BackgroundSubtractorMOG2 was used.
This is an efficient adaptive algorithm that uses Gaussian mixture probability density.
Recursive equations are used to constantly update the parameters and but also to
simultaneously select the appropriate number of components for each pixel [@mog; @mog2].

### Details reducing

For reducing of details that are not interested for us Gaussian Blur filter was used.
The Gaussian Blur is a type of image-blurring filter that uses a Gaussian function
([@eq:gaussian]) for calculating the transformation to apply
to each pixel in the image.

$$ G(x,y)=\frac{1}{2\pi\sigma^2}e^{-\frac{x^2+y^2}{2\sigma^2}} $$ {#eq:gaussian}

* $x$ --- the distance from the origin in the horizontal axis;
* $y$ --- the distance from the origin in the vertical axis;
* $\sigma$ --- the standard deviation of the Gaussian distribution.

The pixels of the filter footprint are weighted using the values got from the 
Gaussian function thus providing a blur effect.
The spacial representation of the Gaussian filter, sometimes referred to as the 
"bell surface", demonstrates how much the individual pixels of the footprint
contribute to the final pixel color.

### Detection and Tracking

After all preprocessing steps detection of a mice is only a contours search.
To retrieve contours from an image algorithm proposed by Suzuki was used [@suzuki].
There are two border following algorithms.
The first one determines the surroundness relations among the borders 
of a binary image.
Since the outer borders and the hole borders have a one-to-one correspondence
to the connected components of 1-pixels and to the holes, respectively,
the proposed algorithm yields a representation of a binary image, from which
one can extract some sort of features without reconstructing the image.
The second algorithm, which is a modified version of the first, follows only
the outermost borders (i.e., the outer borders which are not surrounded by holes).

Despite of small amount of objects, there was a lot of detected ones.
To reduce amount of useless detected objects, were selected only objects which
contour are is larger than 200 and less than 5000 pixels and only that objects
which are inside the previously detected pool.

For mice tracking Kalman Filtering was used.
The Kalman filter for tracking moving objects estimates a state vector comprising
the parameters of the target, such as position and velocity, based on a
dynamic/measurement model.
To use the Kalman filter for the tracking of moving objects, it is necessary
to design a dynamic model of target motion.
The most common dynamic model is a constant velocity (CV) model, which 
assumes that the velocity is constant during a sampling interval.
This model has been used in many applications because of its versatility,
effectiveness, and simplicity [@kalman].
This algorithm allows not only to track mice detection and draw its path,
but also to choose mice from all detected objects.
It is assumed that the longest tracked path belongs to the observed mice.
When mice could not be detected on some frames in case of occlusion,
used tracking algorithm assumes its placement and continues to track
for a small amount of frames.
